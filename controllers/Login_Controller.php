<?php

  session_start();
  include_once  '../utils/session.php'; 

  if(is_logged_in()) {
    include_once '../views/Home_View.php';
    $home = new Home_View();
  }
  else {
    if(!isset($_POST['login']) && !(isset($_POST['pass']))) {
      include '../views/Login_View.php';
      $login = new Login_View();
    }
    else {
      include '../utils/db.php';
      include_once '../models/User_Model.php';

      $enc_pass = md5($_POST['pass']);
      $user_placeholder = new User_Model($_POST['login'], $enc_pass, '', '', '', '');
      $response = $user_placeholder->check_credentials($_POST['login'], $enc_pass);

      if(gettype($response) === "object") {
        if($response->is_active) {
          
          $_SESSION['user'] = $response;

          include_once '../views/Home_View.php';
          $home = new Home_View();
        }
        else {
          include '../views/Login_View.php';
          $login = new Login_View();
        }
      }
      else {
        // ERROR MESSAGE
        die("[ERROR LOGIN] Message: " . $response);
      }

    }
  }




?>