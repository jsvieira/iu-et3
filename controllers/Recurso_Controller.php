<?php

    session_start();
    include_once '../utils/session.php';

    if(!is_logged_in()) {
        include '../views/Login_View.php';
        $login = new Login_View();
    }
    else {
        
        switch($_REQUEST['action']) {
            case 'ADD':

                if(!$_POST) {
                    include_once '../views/recurso/Recurso_Crear_View.php';
                    include_once '../models/Responsable_Model.php';
                    
                    $responsable_placeholder = new Responsable_Model('','','');
                    $responsables = $responsable_placeholder->SEARCH();

                    new Recurso_Crear_View($responsables);
                }
                else {
                    include_once '../models/Recurso_Model.php';
                    $recurso = new Recurso_Model(
                        $_POST['nombre'],
                        $_POST['descripcion'],
                        $_POST['responsable'],
                        (int) $_POST['tarifa'],
                        $_POST['rango_tarifa'],
                        $_POST['borrado']);

                    $response = $recurso->ADD();
                    
                }
            case 'SHOWALL':
                include_once '../views/recurso/Recurso_Showall_View.php';
                new Recurso_Showall_View();
        }
        if($_POST) {
            die("ENDIOS");
            
        }

        


    }




?>