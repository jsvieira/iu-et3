<?php

    session_start();

    include_once  './utils/session.php';

    if(is_logged_in()) { 
        include_once './views/Home_View.php';
        new Home_View();

    }
    else {
        include_once './views/Login_View.php';
        new Login_View();
    }
    


?>