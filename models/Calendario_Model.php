<?php

    class Calendario_Model {
        var $nombre;
        var $descripcion;
        var $fecha_inicio;
        var $fecha_fin;
        var $hora_inicio;
        var $hora_fin;

        public function __construct($nombre, $descripcion, $fecha_inicio, 
            $fecha_fin, $hora_inicio, $hora_fin) {
            $this->nombre = $nombre;
            $this->descripcion = $descripcion;
            $this->fecha_inicio = $fecha_inicio;
            $this->fecha_fin = $fecha_fin;
            $this->hora_inicio = $hora_inicio;
            $this->hora_fin = $hora_fin;

            include_once '../utils/db.php';
            $this->db = connect();
        }

        /// Mandatory functions ///
        public function ADD() {
            
            $sql = "INSERT INTO CALENDARIO (
                NOMBRE_CALENDARIO,
                DESCRIPCION_CALENDARIO,
                FECHA_INICIO_CALENDARIO,
                FECHA_FIN_CALENDARIO,
                HORA_INICIO_CALENDARIO,
                HORA_FIN_CALENDARIO) VALUES (
                '$this->nombre',
                '$this->descripcion',
                '$this->fecha_inicio',
                '$this->fecha_fin',
                '$this->hora_inicio',
                '$this->hora_fin')";

            $response = $this->db->query($sql);
            
            if(!$response) {
                return '[ERROR] No se ha podido insertar el calendario';
            }
            return 'El calendario se ha guardado correctamente';
        }

        public function EDIT() {

        }

        public function DELETE() {

        }

        public function SEARCH() {

        }

    }

?>