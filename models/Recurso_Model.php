<?php

    class Recurso_Model {

        var $nombre;
        var $descripcion;
        var $login_responsable;
        var $tarifa;
        var $rango_tarifa;
        var $borrado;

        function __construct($nombre, $descripcion, $login_responsable, $tarifa,
            $rango_tarifa, $borrado) {
                $this->nombre = $nombre;
                $this->descripcion = $descripcion;
                $this->login_responsable = $login_responsable;
                $this->tarifa = $tarifa;
                $this->rango_tarifa = $rango_tarifa;
                $this->borrado = $borrado;

                include_once '../utils/db.php';
                $this->db = connect();
        }

        /// Mandatory functions ///
        public function ADD() {
            
            $sql = "INSERT INTO RECURSO (
                NOMBRE_RECURSO,
                DESCRIPCION_RECURSO,
                LOGIN_RESPONSABLE,
                TARIFA_RECURSO,
                RANGO_TARIFA_RECURSO,
                BORRADO_LOGICO) VALUES (
                '$this->nombre',
                '$this->descripcion',
                '$this->login_responsable',
                '$this->tarifa',
                '$this->rango_tarifa',
                '$this->borrado')";

            $response = $this->db->query($sql);
            
            if(!$response) {
                return '[ERROR] No se ha podido insertar el recurso';
            }
            return 'El recurso se ha guardado correctamente';
        }

        public function EDIT() {

        }

        public function DELETE() {

        }

        public function SEARCH() {

        }

    }


?>