<?php 
  
  class User_Model {
    var $login;
    var $pass;
    var $name;
    var $email;
    var $is_admin;
    var $is_active;
    
  
    public function __construct($login, $pass, $name, $email, $is_admin, $is_active) {
      $this->login = $login;
      $this->pass = $pass;
      $this->name = $name;
      $this->email = $email;
      $this->is_admin = $is_admin;
      $this->is_active = $is_active;

      include_once '../utils/db.php';
      $this->db = connect();
    }

    /// Mandatory functions ///

    public function ADD() {

    }

    public function EDIT() {

    }

    public function DELETE() {

    }

    public function SEARCH() {

    }

  
    /// Helper functions ///

    public function get_user($login) {
      $sql = "SELECT * FROM USUARIO WHERE LOGIN_USUARIO='$login'";
      $result = $this->db->query($sql);

      if(!$result) {
        return 'Fallo de conexión con la base de datos';
      }
      else if($result->num_rows == 0) {
        return 'El usuario no existe';
      }
      else {
        $object = $result->fetch_assoc();
        return new User_Model(
          $object['LOGIN_USUARIO'],
          $object['PASS_USUARIO'],
          $object['NOMBRE_USUARIO'],
          $object['EMAIL_USUARIO'],
          $this->from_text_to_bool($object['ES_ADMIN']),
          $this->from_text_to_bool($object['ES_ACTIVO']));
      }
    }

    public function check_credentials($login, $pass) {
      $login = mysqli_real_escape_string($this->db, $this->login);
      
      $sql = "SELECT PASS_USUARIO FROM USUARIO WHERE LOGIN_USUARIO='$login'";
      $result = $this->db->query($sql);
      
      if(!$result) {
        return 'Fallo de conexión con la base de datos';
      }
      else if ($result->num_rows == 0) {
        return 'Fallo de autenticación';
      }
      else {
        if($pass == $result->fetch_assoc()['PASS_USUARIO']) {
          return $this->get_user($login);
        } else {
          return 'Fallo de autenticación';
        }
      }
    }


    /// Utils functions ///
    private function from_text_to_bool($text) {
      if($text === "NO") {
          return false;
      }
      else if ($text === "SI") {
          return true;
      }
      else{
          return -1;
      }
  }
  }

?>