<?php
    
    session_start();
    session_destroy();
    include_once '../views/Login_View.php';
    new Login_View();
    header('Location: ../index.php');
?>
