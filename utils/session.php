<?php

    function is_logged_in() {
        if(isset($_SESSION['user'])) {
            return true;
        }
        else {
            return false;
        }   
    }

    function log_out() {
        if(isset($_SESSION['user'])) {
            session_destroy();
            include_once '../views/Login_View';
        }
    }
?>