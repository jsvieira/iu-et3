<?php

  class Login_View {

    function __construct() {
      $this->execute();
    }

    function execute() {
?>
      <html>
        <head>
        <link href="<?php echo dirname('page_file_temp'); ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo dirname('page_file_temp'); ?>/css/signin.css" rel="stylesheet">
        </head>
        <body class="text-center">
          <main class="form-signin">
            <form method="POST" action="<?php echo dirname('__FILE__'); ?>/controllers/Login_Controller.php">
              <img class="mb-4" src="<?php echo dirname('__FILE__'); ?>/assets/brand-logo.png" alt="" width="72" height="57">
              <h1 class="h3 mb-3 fw-normal" style="color:white">Inicie sesión</h1>
              <label for="inputLogin" class="visually-hidden">Login usuario</label>
              <input type="text" id="inputLogin" name="login" class="form-control" placeholder="Login usuario" required autofocus>
              <label for="inputPassword" class="visually-hidden">Contraseña</label>
              <input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Contraseña" required>
              <button class="w-100 btn btn-lg btn-primary" type="submit">Entrar</button>
              <p class="mt-5 mb-3 text-muted">IU ET3 2020-2021</p>
            </form>
          </main>
          <script src="<?php echo dirname('__FILE__'); ?>/js/bootstrap.bundle.min.js"></script>
        </body>
      </html>
<?php
    }



  }

?>