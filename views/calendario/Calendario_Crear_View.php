<?php
    class Calendario_Crear_View {
        
        function __construct() {
            $this->execute();
        }

        function execute() {
            
            include_once __DIR__ . '/../templates/header.php';
?>
            <script>

                document.addEventListener('DOMContentLoaded', function() {
                var calendarEl = document.getElementById('calendar');
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    height: 500,
                    firstDay: 1,
                    initialView: 'timeGridWeek',
                    allDaySlot: false,
                    nowIndicator: true,
                    headerToolbar:{
                        left:'prev,next today',
                        center:'title',
                        end:'timeGridWeek,dayGridMonth'
                    },
                    selectable: true,
                    selectMirror: true,
                    select: function(info) {
                        console.log($("#fecha_inicio"))
                        var year_start = info.start.getFullYear()
                        var month_start = info.start.getMonth() + 1
                        var year_end = info.end.getFullYear()
                        var month_end = info.end.getMonth() + 1
                        var fecha_inicio = year_start + "-" + month_start + "-" + info.start.getDate()
                        var fecha_fin = year_end + "-" + month_end + "-" + info.end.getDate()
                        var hora_inicio = info.start.getHours() + ":" + info.start.getMinutes()
                        var hora_fin = info.end.getHours() + ":" + info.end.getMinutes()
                        
                        $("#fecha_inicio").val(fecha_inicio)
                        $("#fecha_fin").val(fecha_fin)
                        $("#hora_inicio").val(hora_inicio)
                        $("#hora_fin").val(hora_fin)
                    }
                });
                calendar.render();
                });

            </script>
            <h1 class="h2">Crear calendario</h1>
            </div>
            <form method="POST" action='/et3/controllers/Calendario_Controller.php?action=ADD'>
            <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" name="nombre" placeholder="Nombre">
            </div>
            <div class="form-group">
                <label>Descripción</label>
                <input type="text" class="form-control" name="descripcion" placeholder="Descripción">
            </div>
            <div class="form-group">
                <input type="hidden" class="form-control" id="fecha_inicio" name="fecha_inicio" value="">
                <input type="hidden" class="form-control" id="fecha_fin" name="fecha_fin" value="">
                <input type="hidden" class="form-control" id="hora_inicio" name="hora_inicio" value="">
                <input type="hidden" class="form-control" id="hora_fin" name="hora_fin" value="">
            </div>
            <div id="calendar"></div>
            
            <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
<?php
            include_once __DIR__ . '/../templates/footer.php';
        }
    }
?>