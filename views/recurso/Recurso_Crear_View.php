<?php
    class Recurso_Crear_View {
        
        var $responsables;

        function __construct($responsables) {
            $this->responsables = $responsables;

            $this->execute();
        }

        function execute() {
            
            include_once __DIR__ . '/../templates/header.php';
?>
            <h1 class="h2">Crear recurso</h1>
            </div>
            <form method="POST" action='/et3/controllers/Recurso_Controller.php?action=ADD'>
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="nombre" placeholder="Nombre">
                </div>
                <div class="form-group">
                    <label>Descripción</label>
                    <input type="text" class="form-control" name="descripcion" placeholder="Descripción">
                </div>
                <div class="form-group">
                    <label>Responsable</label>
                    <select class="form-select" name="responsable">
                        <option value="" selected>-- Login Responsable --</option>
                        <?php
                            while(($row = mysqli_fetch_array($this->responsables))) {
                                echo('<option value="'. $row['LOGIN_RESPONSABLE'] .'">'. $row['LOGIN_RESPONSABLE'] .'</option>');
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tarifa</label>
                    <input type="number" class="form-control" name="tarifa" placeholder="Tarifa">
                </div>
                <div class="form-group">
                    <label>Rango tarifa</label>
                    <select class="form-select" name="rango_tarifa">
                        <option selected>-- Rango Tarifa --</option>
                        <option value="HORA">Hora</option>
                        <option value="DIA">Día</option>
                        <option value="SEMANA">Semana</option>
                        <option value="MES">Mes</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Borrado lógico</label><br>
                    <select class="form-select" name="borrado">
                        <option value="NO" selected>No</option>
                        <option value="SI">Si</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
<?php
            include_once __DIR__ . '/../templates/footer.php';
        }
    }
?>